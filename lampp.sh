#!/bin/bash

lampp="/opt/lampp"

dst="$lampp/htdocs/rgb-ui"
lamppExe="$lampp/lampp"

if [ -d "$dst" ]
then
  sudo rm -r "$dst"
fi

sudo mkdir "$dst"
sudo chown $USER: "$dst"

cp -r "rgb-ui" "$dst/"

cd "./example/"
cp -r * "$dst/"

if [[ $(echo $(sudo "$lamppExe" status)) =~ "Apache is not running" ]]
then
  sudo "$lamppExe" startapache
fi

$(nohup firefox -new-tab "http://localhost/rgb-ui/index.php" > /dev/null 2>&1 &)