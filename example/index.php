<?php
  require 'rgb-ui/rgb-ui.php';
?>

<!DOCTYPE html>
<html lang="<?php echo($currentLanguage); ?>">
  <head>
    <meta charset="utf-8"><!--https://developer.mozilla.org/en-US/docs/Web/Guide/HTML/Obsolete_things_to_avoid#%3Cmeta%3E_element_and_charset_attribute-->
    <title><?php echoLocalized("SITE_NAME"); ?> - <?php echoLocalized("Page title"); ?></title>
    <?php rgbui_writeHead(); ?>
  </head>
  
  <body>
    <?php
      rgbui_start();
    ?>
    
    <?php echoLocalized("Home page"); ?>
    
    <?php
      rgbui_end();
    ?>
  </body>
</html>