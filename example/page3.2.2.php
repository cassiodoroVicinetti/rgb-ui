<?php
  require 'rgb-ui/rgb-ui.php';
?>

<!DOCTYPE html>
<html lang="<?php echo($currentLanguage); ?>">
  <head>
    <meta charset="utf-8">
    <title><?php echoLocalized("SITE_NAME"); ?> - <?php echoLocalized("Page title"); ?></title>
    <?php rgbui_writeHead(); ?>
  </head>
  
  <body>
    <?php
      rgbui_start();
    ?>
    
    <?php echoLocalized("Page 3.2.2"); ?> (test string: <?php echoLocalized("Test string"); ?>)
    
    <?php
      rgbui_end();
    ?>
  </body>
</html>