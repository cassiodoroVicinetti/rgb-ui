<?php
  function writeFooterCss() {
?>

    <link rel="stylesheet" type="text/css" href="<?php echo(PREFIX_PATH); ?>rgb-ui/css/footer.css">

<?php
  }

  function writeFooter() {
    global $paramsXml, $currentLanguage, $localizationStructure;
?>

    <footer id="footer">
    
<?php

      $baseUrl = getBaseUrl();
      $first = true;
      
      foreach ($paramsXml->footer->children() as $entry) {
	if (!$first) {
	  echo(" &#x25C6; ");
	} else
	  $first = false;
      
	$url = getXmlTagValue($entry);
      
	$text = getXmlTagAttribute($entry, "text");
	if (getXmlTagAttribute($entry, "localize") != "false")
	  $text = getLocalized($text);
	  
	$external = (getXmlTagAttribute($entry, "external") == "true");
	$newTab = (getXmlTagAttribute($entry, "newtab") == "true");
	
	if ($external) {
?>

	  <a href="<?php
	    echo($url);
	  ?>"<?php
	    if ($newTab)
	      echo(" target='_blank'");
	  ?>><?php
	    echo($text);
	  ?></a>

<?php
	} else {
	  $link = ($baseUrl != PREFIX_PATH . $url);
	  if ($link) {
?>

	    <a href="<?php
	      echo(PREFIX_PATH . $url);
	    ?>"<?php
	      if ($newTab)
		echo(" target='_blank'");
	    ?>><?php
	      echo($text);
	    ?></a>

<?php
	  } else {
?>

	    <span style="font-weight: bold;"><?php echo($text); ?></span>

<?php
	  }
	}
	
      }

	if (count($localizationStructure->availableLanguages) > 1) {
      ?>
      
      <form><!--http://stackoverflow.com/a/16800092-->
	<div style="text-align: center;">
	  <?php echoLocalized("LANGUAGE"); ?>: <select name="setlang" onchange="this.form.submit();"><!--http://stackoverflow.com/a/16244264-->
	    <?php
	      foreach ($localizationStructure->availableLanguages as $language) {
		?>
		<option value="<?php
		  echo($language);
		?>"<?php
		  if ($language == $currentLanguage)
		    echo(" selected");
		?>><?php echo($localizationStructure->codeToString[$language]); ?></option>
		<?php
	      }
	    ?>
	  </select>
	  
	  <?php
	    foreach ($_GET as $getKey => $getValue) {
	      if ($getKey != "setlang") {
		?>
		<input type="hidden" name="<?php echo($getKey); ?>" value="<?php echo($getValue); ?>">
		<?php
	      }
	    }
	  ?>
	</div>
      </form>
      
      <?php
        }
      ?>

    </footer>

<?php
  }
?>