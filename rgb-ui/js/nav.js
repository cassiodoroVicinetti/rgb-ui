function toggleFolder(obj) {
  var parent = $(obj).parent();
  
  if (parent.hasClass("open")) // header red open
    setCookie(parent[0].id, "close");
  else // header red
    setCookie(parent[0].id, "open");
  
  parent.toggleClass("open");
}