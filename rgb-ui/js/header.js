function toggleNav() {
  var nav = $("#nav");
  nav.toggleClass("navHidden");
  $("#topLeftArea").toggleClass("navHidden");
  $("#content").toggleClass("navHidden");
  
  if (nav.hasClass("navHidden")) {
    $("#navToggleButton")[0].value = ">>";
    setCookie("navHidden", "1");
    
  } else {
    $("#navToggleButton")[0].value = "<<";
    setCookie("navHidden", "0");
  }
}