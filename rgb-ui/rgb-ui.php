<?php
  define("PREFIX_DIR", dirname(dirname(__FILE__)) . "/");
  
  require PREFIX_DIR . 'rgb-ui/lib/common.php';
  
  $paramsXml = simplexml_load_file(PREFIX_DIR . 'params.xml');
  
  foreach($paramsXml->constants->children() as $constant) {
    define(getXmlTagAttribute($constant, "name"), getXmlTagValue($constant));
  }
  
  ini_set("session.cookie_path", PREFIX_PATH);
  session_start();
  
  $colors = array();
  if (RED_COLOR)
    $colors[] = "red";
  if (GREEN_COLOR)
    $colors[] = "green";
  if (BLUE_COLOR)
    $colors[] = "blue";
  
  require PREFIX_DIR . 'rgb-ui/lib/lang.php';
  initLang($paramsXml);
  
  require PREFIX_DIR . 'rgb-ui/header.php';
  
  require PREFIX_DIR . 'rgb-ui/nav.php';
  initNav($paramsXml, $colors);
  
  require PREFIX_DIR . 'rgb-ui/footer.php';
  
  function rgbui_writeHead() {
?>

    <meta name="viewport" content="width=device-width, initial-scale=1">
    
<?php
    if (PRODUCTION) {
?>
  
    <link type="text/css" href="https://cdn.jsdelivr.net/npm/bootstrap@<?php echo(BOOTSTRAP_VERSION); ?>/dist/css/bootstrap.min.css" rel="stylesheet" integrity="<?php echo(BOOTSTRAP_INTEGRITY); ?>" crossorigin="anonymous">
  
    <script src="http://code.jquery.com/jquery-<?php echo(JQUERY_VERSION); ?>.min.js"></script>
  
<?php
    } else {
?>
  
    <link type="text/css" href="https://cdn.jsdelivr.net/npm/bootstrap@<?php echo(BOOTSTRAP_VERSION); ?>/dist/css/bootstrap.css" rel="stylesheet" crossorigin="anonymous">

    <script src="http://code.jquery.com/jquery-<?php echo(JQUERY_VERSION); ?>.js"></script>
  
<?php
    }
      
?>

    <link rel="stylesheet" type="text/css" href="<?php echo(PREFIX_PATH); ?>rgb-ui/css/content.css">
    
<?php
    writeHeaderCss();
    writeNavCss();
    writeFooterCss();
?>

    <script>
      var PREFIX_PATH = "<?php echo(PREFIX_PATH); ?>";
    </script>
    
    <script src="<?php echo(PREFIX_PATH); ?>rgb-ui/js/common.js"></script>
    
<?php
    writeHeaderJs();
    writeNavJs();
  }
  
  function rgbui_start() {
    global $colors;
    $navHidden = (isset($_COOKIE["navHidden"]) && $_COOKIE["navHidden"] == "1");
    writeHeader($navHidden, $colors);
?>

    <div id="navContainer">
    
<?php
    writeNav($navHidden);
?>

      <div id="content"<?php
	if ($navHidden)
	  echo(" class=\"navHidden\"");
      ?>>
    
<?php
  }

  function rgbui_end() {
?>
      </div>
    </div>
<?php

    writeFooter();
  }
?>
