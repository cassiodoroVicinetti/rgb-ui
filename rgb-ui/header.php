<?php
  function header_writeColors($string, $colors, &$colorIndex) {
    $chars = str_split($string);
    $colorsLength = count($colors);
    
    foreach ($chars as $char) {
      echo("<span class=" . $colors[$colorIndex] . ">" . $char . "</span>");
      $colorIndex = ($colorIndex + 1) % $colorsLength;
    }
  }
    
  function writeHeaderCss() {
?>

    <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Amaranth:b">
    <link rel="stylesheet" type="text/css" href="<?php echo(PREFIX_PATH); ?>rgb-ui/css/header.css">

<?php
  }
  
  function writeHeaderJs() {
?>

    <script src="<?php echo(PREFIX_PATH); ?>rgb-ui/js/header.js"></script>

<?php
  }
    
  function writeHeader($navHidden, $colors) {
    $colorIndex = 0;
?>

    <header id="header">
    
      <div id="topLeftArea"<?php
	if ($navHidden)
	  echo(" class=\"navHidden\"");
      ?>></div>
      
      <div id="topArea"></div>
      
      <div id="topRightArea"></div>
      
      <a href="<?php echo(PREFIX_PATH); ?>index.php" id="headerText">
	<?php
	  header_writeColors(getLocalized("HEADER"), $colors, $colorIndex);
	?>
      </a>
      
      <div id="toggleButtonArea">
	<input type="button" onclick="javascript:toggleNav();" id="navToggleButton" value="<?php
	if ($navHidden)
	  echo(">>");
	else
	  echo("<<");
	?>">
      </div>
      
    </header>

<?php
  }
?>