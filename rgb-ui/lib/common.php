<?php
  function getBaseUrl() {
    return $_SERVER["PHP_SELF"];
  }
  
  function getXmlTagAttribute($tag, $key) {
    $attributes = $tag->attributes();
    return (string)$attributes[$key];
  }
  
  function getXmlTagValue($tag) {
    $value = (string)$tag;
    
    switch (getXmlTagAttribute($tag, "type")) {
      case "bool":
	// http://stackoverflow.com/a/6537960/1267803
	switch ($value) {
	  case "true":
	    return true;
	  default:
	    return false;
	}
	
      default:
	return $value;
    }
  }
?>