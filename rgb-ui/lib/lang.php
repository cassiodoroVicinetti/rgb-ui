<?php
  $localizationStructure;
  $currentLanguage;
  
  function initLang($paramsXml) {
    global $localizationStructure, $currentLanguage;
    $localizationStructure = new LocalizationStructure();
    
    foreach ($paramsXml->languages->children() as $language) {
      $languageString = getXmlTagValue($language);
      $languageCode = getXmlTagAttribute($language, "code");
      $localizationStructure->codeToString[$languageCode] = $languageString;
      if (getXmlTagAttribute($language, "default") == "true")
	array_unshift($localizationStructure->availableLanguages, $languageCode);
      else
	array_push($localizationStructure->availableLanguages, $languageCode);
    }
  
    if (isset($_GET["setlang"]) && in_array($_GET["setlang"], $localizationStructure->availableLanguages)) {
      $currentLanguage = $_GET["setlang"];
      $_SESSION["language"] = $currentLanguage;
      
    } else if (isset($_SESSION["language"]))
      $currentLanguage = $_SESSION["language"];
      
    else
      $currentLanguage = NULL;
      
    $matches = array();
      
    $tags = explode(",", $_SERVER['HTTP_ACCEPT_LANGUAGE']);
      
    foreach ($tags as $tag) {
      initLang_inner1($tag, $localizationStructure->availableLanguages, $matches, $currentLanguage);
    }
      
    arsort($matches);
      
    $localizationStructure->languages = array_keys($matches);
    
    if ($currentLanguage != NULL)
      array_unshift($localizationStructure->languages, $currentLanguage);
      
    // The first language is the default language
    if (!in_array($localizationStructure->availableLanguages[0], $localizationStructure->languages))
      array_push($localizationStructure->languages, $localizationStructure->availableLanguages[0]); // default language
      
    if ($currentLanguage == NULL)
      $currentLanguage = $localizationStructure->languages[0];
      
    foreach ($localizationStructure->languages as $lang) {
      $localizationStructure->localizedStrings[$lang] = new LocalizedStringCollection();
      $langXml = simplexml_load_file(PREFIX_DIR . 'languages/' . $lang . '.xml');
      foreach ($langXml->children() as $scope) {
	if ($localizationStructure->localizedStrings[$lang]->newLocalizedScope(explode(" ", getXmlTagAttribute($scope, "subscopes")))) {
	  foreach ($scope->children() as $string) {
	    $localizationStructure->localizedStrings[$lang]->setLocalized(getXmlTagAttribute($string, "key"), getXmlTagValue($string));
	  }
	}
      }
    }
  }
  
  class LocalizationStructure {
    public $localizedStrings;
    public $languages;
    public $availableLanguages;
    public $codeToString;
    
    public function __construct() {
      $this->localizedStrings = array();
      $this->availableLanguages = array();
      $this->codeToString = array();
    }
  }
  
  class LocalizedStringSubCollection {
    public $strings, $valid;
      
    public function __construct($scopes) {
      $baseUrl = getBaseUrl();
      $this->valid = false;
      
      foreach ($scopes as $scope) {
	if (($scope == "*") || (PREFIX_PATH . $scope == $baseUrl)) {
	  $this->valid = true;
	  $this->strings = array();
	  return;
	}
      }
    }
    
    public function setLocalized($key, $value) {
      $this->strings[$key] = $value;
    }
    
    public function getLocalized($key, &$value) {
      if (!isset($this->strings[$key]))
	return false;
      $value = $this->strings[$key];
      return true;
    }
  }
  
  class LocalizedStringCollection {
    public $localizedStringSubCollections;
    
    public function __construct() {
      $this->localizedStringSubCollections = array();
    }
    
    public function newLocalizedScope($scopes) {
      $subCollection = new LocalizedStringSubCollection($scopes);
      if (!$subCollection->valid)
	return false;
	
      array_push($this->localizedStringSubCollections, $subCollection);
      return true;
    }
    
    public function setLocalized($key, $value) {
      $this->localizedStringSubCollections[count($this->localizedStringSubCollections) - 1]->setLocalized($key, $value);
    }
    
    public function getLocalized($key, &$value) {
      foreach ($this->localizedStringSubCollections as $subCollection) {
	if ($subCollection->getLocalized($key, $value))
	  return true;
      }
      return false;
    }
  }
    
  function initLang_inner2(&$matches, $lang, $q, $currentLanguage) {
    if ($currentLanguage != NULL && $lang == $currentLanguage)
      return;
    
    foreach ($matches as $matchesLang => $matchesQ) {
      if ($matchesLang == $lang) {
	if ($matchesQ < $q)
	  $matches[$matchesLang] = $q;
	// else: $matchesItem["q"] >= $q
	return;
      }
    }
      
    $matches[$lang] = $q;
  }
    
  function initLang_inner1($tag, $available, &$matches, $currentLanguage) {
    // $tag = "en-us;q=0.8"
    list($langField, $qField) = array_pad(explode(";", $tag), 2, NULL); // http://stackoverflow.com/a/6576365
    
    // $langField = "en-us"
    $langSubfields = explode("-", $langField);
    $lang = strtolower(trim($langSubfields[0]));
    
    // $lang = "en"
    if ($lang != "*" && !in_array($lang, $available))
      return;
    
    $q;
    if ($qField == NULL)
      $q = 1.0;
    else {
      // $qField = "q=0.8"
      $qSubfields = explode("=", $qField);
      $q = floatval($qSubfields[1]);
    }
    
    if ($lang == "*") {
      foreach ($available as $availableLang) {
	initLang_inner2($matches, $availableLang, $q, $currentLanguage);
      }
    } else
      initLang_inner2($matches, $lang, $q, $currentLanguage);
  }
  
  function getLocalized($key) {
    global $localizationStructure;
    foreach ($localizationStructure->languages as $lang) {
      $value;
      if ($localizationStructure->localizedStrings[$lang]->getLocalized($key, $value)) {
      
	$numargs = func_num_args();
	$arg_list = func_get_args();
	for ($i = 1; $i < $numargs; $i++)
	  $value = str_replace("{" . $i . "}", $arg_list[$i], $value);
	  
	return $value;
      }
    }
    return $key; // not found
  }
  
  function echoLocalized($key) {
    $args = func_get_args(); // http://stackoverflow.com/a/2126790
    echo(call_user_func_array("getLocalized", $args));
  }
?>