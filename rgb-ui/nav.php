<?php
  $navText = "";
  
  function initNav($paramsXml, $colors) {
    global $navText;
    $navButtons = array();
    
    foreach ($paramsXml->nav->children() as $entry) {
      initNav_entry($entry, $navButtons);
    }
    
    $baseUrl = getBaseUrl();
    $id = 1;
    $colorIndex = 0;
    $colorsLength = count($colors);
    
    foreach ($navButtons as $navButton) {
      $navText .= getNavButtonText($navButton, $id, $colors[$colorIndex], $baseUrl);
      $colorIndex = ($colorIndex + 1) % $colorsLength;
      $id++;
    }
  }
  
  function initNav_entry($entry, &$parentArray) {
    $text = getXmlTagAttribute($entry, "text");
    if (getXmlTagAttribute($entry, "localize") != "false")
      $text = getLocalized($text);
    
    if ($entry->getName() == "item")
      initNav_item($entry, $parentArray, $text);
    else // folder
      initNav_folder($entry, $parentArray, $text);
  }
  
  function initNav_item($item, &$parentArray, $text) {
    $external = (getXmlTagAttribute($item, "external") == "true");
    $newTab = (getXmlTagAttribute($item, "newtab") == "true");
    array_push($parentArray, new NavItem(getXmlTagValue($item), $text, $external, $newTab));
  }
  
  function initNav_folder($folder, &$parentArray, $text) {
    $navFolder = new NavFolder($text);
    foreach ($folder as $entry) {
      initNav_entry($entry, $navFolder->entries);
    }
    array_push($parentArray, $navFolder);
  }

  class NavItem {
    public $url, $text, $external, $newTab;
    
    public function __construct($url, $text, $external, $newTab) {
      $this->url = $url;
      $this->text = $text;
      $this->external = $external;
      $this->newTab = $newTab;
    }
  }
  
  class NavFolder {
    public $text, $entries;
    
    public function __construct($text) {
      $this->text = $text;
      $this->entries = array();
    }
  }

  function getNavItemText($navItem, $color, $baseUrl, $level, &$forceOpen) {
    $url = $navItem->url;
    $text = $navItem->text;
    $external = $navItem->external;
    $newTab = $navItem->newTab;
    
    $itemText = "<li class='item level" . $level . (($level == 0) ? " " . $color : "") . "'>";
    
    $target = ($newTab) ? " target='_blank'" : "";
    
    if ($external)
      $itemText .= "<a href='" . $url . "'" . $target . ">" . $text . "</a>";
    
    else {
      $forceOpen = ($baseUrl == PREFIX_PATH . $url . ((strlen($url) === 0 || substr($url, -1) === "/") ? "index.php" : ""));
      
      if ($forceOpen)
        $itemText .= "<span>" . $text . "</span>";
      else
        $itemText .= "<a href='" . PREFIX_PATH . $url . "'" . $target . ">" . $text . "</a>";
    }
    
    $itemText .= "</li>";
    
    return $itemText;
  }
  
  function getNavFolderText($navFolder, $prefixId, $id, $color, $baseUrl, $level, &$forceOpen) {
    $forceOpen = false;
    $subText = "";
    $subId = 1;
    
    foreach ($navFolder->entries as $entry) {
      if ($forceOpen) {
        $junk; // Only variables can be passed by reference.
        $subText .= getNavEntryText($entry, $prefixId . $id . "_", $subId, $color, $baseUrl, $level + 1, $junk);
      } else
        $subText .= getNavEntryText($entry, $prefixId . $id . "_", $subId, $color, $baseUrl, $level + 1, $forceOpen);
      $subId++;
    }
    
    $idTag = "nav" . $prefixId . $id;
    
    if ($forceOpen) {
      $open = true;
      setcookie($idTag, "open", 0, PREFIX_PATH);
    } else
      $open = (isset($_COOKIE[$idTag]) && $_COOKIE[$idTag] == "open");
    
    $folderText = "<li id='" . $idTag . "' class='folder level" . $level . (($level == 0) ? " " . $color : "") . (($open) ? " open" : "") . "'>";
    
    if ($forceOpen)
      $folderText .= "<span>";
    else
      $folderText .= "<a href='javascript:void(0);' onclick='javascript:toggleFolder(this);'>"; // http://stackoverflow.com/a/15885150
    
    $folderText .= $navFolder->text;
    
    $folderText .= "</" . (($forceOpen) ? "span" : "a") . ">";
    
    $folderText .= "<ul>";
    
    $folderText .= $subText;
    
    $folderText .= "</ul>";
    
    if ($level == 0 && !$forceOpen)
      $folderText .= "<span class='shadow'></span>";
    
    $folderText .= "</li>";
    
    return $folderText;
  }
  
  function getNavEntryText($navEntry, $prefixId, $id, $color, $baseUrl, $level, &$forceOpen) {
    if ($navEntry instanceof NavItem)
      return getNavItemText($navEntry, $color, $baseUrl, $level, $forceOpen);
    else // NavFolder
      return getNavFolderText($navEntry, $prefixId, $id, $color, $baseUrl, $level, $forceOpen);
  }
  
  function getNavButtonText($navButton, $id, $color, $baseUrl) {
    $junk; // Only variables can be passed by reference.
    return getNavEntryText($navButton, "", $id, $color, $baseUrl, 0, $junk);
  }
  
  function writeNavCss() {
?>

    <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Delius">
    <link rel="stylesheet" type="text/css" href="<?php echo(PREFIX_PATH); ?>rgb-ui/css/nav.css">

<?php
  }
  
  function writeNavJs() {
?>

    <script src="<?php echo(PREFIX_PATH); ?>rgb-ui/js/nav.js"></script>

<?php
  }
  
  function writeNav($navHidden) {
    global $navText;
?>

    <nav id="nav"<?php
      if ($navHidden)
	echo(" class=\"navHidden\"");
    ?>>
      <div id="list">
	<ul>
	
	  <?php
	    echo($navText);
	  ?>
	  
	</ul>
      </div>
      
      <div id="bottomLeftArea"></div>
    </nav>

<?php
  }
?>